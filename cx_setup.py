from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(
    packages = ['rabbitvcs', 'gtk'], # 'gio'],
    includes = ['atk', 'gtkunixprint', 'pango', 'pangocairo', 'dsextras'],
    # excludes = ['Tkinter'],
    include_files = [('rabbitvcs/ui/xml/', 'xml/'),
                     ('data/', 'data/'),
                     ('rabbitvcs/util/configspec/', 'configspec/'),
                     "AUTHORS",

    ]
)

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('clients/cli/rabbitvcs', base=base,
               icon='data/icons/hicolor/scalable/apps/rabbitvcs.svg'
    )
]

setup(name='frozenrabbit',
      version = '1.0',
      description = 'Frozen version of RabbitVCS',
      options = dict(build_exe = buildOptions),
      executables = executables)
