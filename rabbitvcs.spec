# -*- mode: python -*-
from PyInstaller.utils.hooks import collect_submodules
block_cipher = None

hiddenimports= ['rabbitvcs',
                'rabbitvcs.util', 
                'gtk', 'gtk._gtk',
                'atk', 'gtkunixprint',
                'pango', 'pangocairo', 'dsextras']
hiddenimports += collect_submodules('rabbitvcs.ui')

datas=[('rabbitvcs/ui/xml/', 'xml/'),
       ('data/', 'data/'),
       ('rabbitvcs/util/configspec/', 'configspec/'),
       ('AUTHORS', '.'),
]

base_dir = '.' # Added
gtks = ['loaders.cache', 'pangorc', 'pango.modules', 'pangox.aliases'] # Added
#data_files = [(x, os.path.join(base_dir, x), 'DATA') for x in gtks] # Added
data_files = [('etc/pangorc', 'pangorc', 'DATA'),
              ('etc/pango.modules', 'pango.modules', 'DATA'),
              ('etc/pangox.aliases', 'pangox.aliases', 'DATA'),
              ('lib/gdk-pixbuf/loaders.cache', 'loaders.cache', 'DATA')]
              

more_binaries = [] # Added
pixbuf_dir = '/hometmp/satya/src/py27/lib/gtk-2.0/2.10.0/loaders' # Added
for pixbuf_type in os.listdir(pixbuf_dir): # Added
    if pixbuf_type.endswith('.so'): # Added
        more_binaries.append((os.path.join('lib', 'gdk-pixbuf', pixbuf_type),
                              os.path.join(pixbuf_dir, pixbuf_type),
                              'BINARY')) # Added

pango_dir = '/hometmp/satya/src/py27/lib/pango/1.6.0/modules' # Added
for pango_type in os.listdir(pango_dir): # Added
    if pango_type.endswith('.so'): # Added
        more_binaries.append((os.path.join('pango/1.6.0/modules', pango_type), os.path.join(pango_dir, pango_type), 'BINARY')) # Added



a = Analysis(['clients/cli/rabbitvcs'],
             pathex=['/hometmp/satya/src/rabbitvcs'],
             binaries=[],
             datas=datas,
             hiddenimports = hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='rabbitvcs',
          debug=False,
          strip=False,
          upx=False,
          console=True )
coll = COLLECT(exe, data_files,
               a.binaries + more_binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='rabbitvcs')
