import os, sys

os.environ['PANGO_LIBDIR'] = sys._MEIPASS
os.environ['PANGO_SYSCONFDIR'] = os.path.join(sys._MEIPASS, 'etc') # TODO?
os.environ['PANGO_RC_FILE'] = os.path.join(sys._MEIPASS, 'etc', 'pangorc')
os.environ['GDK_PIXBUF_MODULE_FILE'] = os.path.join(sys._MEIPASS, 'lib',
                                                    'gdk-pixbuf', 
                                                    'loaders.cache')
